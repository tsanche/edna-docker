FROM rocker/r-ver:4.2.2
LABEL version="0.17" 
LABEL maintainer="Théophile Sanchez <theophile.sanchez@wsl.ch>"

ENV S6_VERSION=v2.1.0.2
ENV RSTUDIO_VERSION=daily
ENV DEFAULT_USER=rstudio
ENV PANDOC_VERSION=default
ENV PATH=/usr/lib/rstudio-server/bin:$PATH

# install base packages
RUN apt-get update -y
RUN apt-get install -y build-essential
RUN apt-get install -y wget htop vim nano man cmake git

# install python 
RUN apt-get install -y python2
RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN wget https://bootstrap.pypa.io/pip/2.7/get-pip.py
RUN python2 get-pip.py

# dependencies
RUN apt-get update -y
RUN apt-get install -y mc zlib1g zlib1g-dev libghc-bzlib-dev liblzma-dev libxml2 libxml2-dev libigraph-dev autoconf python3-pyqt5 ecopcr python2.7-dev libpng-dev libudunits2-dev libgdal-dev libgeos-dev libproj-dev 
RUN python3 -m pip install --upgrade cutadapt
RUN pip2 install cython pygments==2.5.2 sphinxcontrib-websupport==1.1.2 babel Jinja2 'requests<2.28' packaging traitlets alabaster pexpect sphinx virtualenv==16.7.9

# install R packages
RUN /rocker_scripts/install_rstudio.sh
RUN /rocker_scripts/install_pandoc.sh
RUN R -e "install.packages('tidyverse')"
RUN R -e "install.packages('BiocManager')"
RUN R -e "BiocManager::install('ShortRead')"
RUN R -e "BiocManager::install('Biostrings')"
RUN R -e "BiocManager::install('phyloseq')"
RUN R -e "BiocManager::install('dada2')"
RUN R -e "download.file(url='https://cran.r-project.org/src/contrib/Archive/PhyloMeasures/PhyloMeasures_2.1.tar.gz', destfile='PhyloMeasures_2.1.tar.gz')"
RUN R -e "install.packages(pkgs='PhyloMeasures_2.1.tar.gz', type='source', repos=NULL)"
RUN rm PhyloMeasures_2.1.tar.gz
RUN R -e "install.packages('ape')"
RUN R -e "install.packages('mFD')"
RUN R -e "install.packages('worrms')"
RUN R -e "install.packages('patchwork')"
RUN R -e "install.packages('hillR')"
RUN R -e "install.packages('betapart')"
RUN R -e "download.file(url='https://cran.r-project.org/src/contrib/Archive/seqRFLP/seqRFLP_1.0.1.tar.gz', destfile='seqRFLP_1.0.1.tar.gz')"
RUN R -e "install.packages(pkgs='seqRFLP_1.0.1.tar.gz', type='source', repos=NULL)"
RUN rm seqRFLP_1.0.1.tar.gz
RUN R -e "install.packages('devtools')"
RUN R -e "install.packages('sf')"


# download sources
WORKDIR /app

RUN wget https://gitlab.mbb.cnrs.fr/edna/custom_reference_database/-/archive/v1.1/custom_reference_database-v1.1.tar.gz
RUN tar -zxvf custom_reference_database-v1.1.tar.gz

RUN wget https://github.com/torognes/vsearch/archive/v2.22.1.tar.gz
RUN tar -zxvf v2.22.1.tar.gz

RUN git -c http.sslVerify=false clone https://git.metabarcoding.org/obitools/obitools.git OBITools

RUN wget --no-check-certificate https://git.metabarcoding.org/obitools/ecoprimers/uploads/40f0fe1896a15ca9ad29835f93893464/ecoPrimers.tar.gz
RUN tar -zxvf ecoPrimers.tar.gz

# install MKBDR
WORKDIR /app/custom_reference_database-v1.1
RUN pip3 install .

# install vsearch
WORKDIR /app/vsearch-2.22.1
RUN ./autogen.sh
RUN ./configure CFLAGS="-O3" CXXFLAGS="-O3"
RUN make
RUN make install 

# install obitools
WORKDIR /app/OBITools/
ARG TARGETPLATFORM
RUN if [ "$TARGETPLATFORM" = "linux/arm64" ]; then sed -i "/sse2/d" setup.py; fi
WORKDIR /app/OBITools/distutils.ext/obidistutils
RUN if [ "$TARGETPLATFORM" = "linux/arm64" ]; then sed -i "/sse2/d" core.py; fi
WORKDIR /app/OBITools
RUN python2 setup.py install

# install ecoPrimers
WORKDIR /app/ecoprimers/src/
RUN if [ "$TARGETPLATFORM" = "linux/arm64" ]; then sed -e "s/-m64 //" -i Makefile; fi
RUN if [ "$TARGETPLATFORM" = "linux/arm64" ]; then sed -e "s/-m64 //" -i global.mk; fi
RUN make
RUN echo "export PATH=$PATH:/app/ecoprimers/src/" >> /home/rstudio/.bashrc

# cleanup
WORKDIR /app
RUN rm *.tar.gz* 
RUN rm -rf /var/lib/apt/lists/* 
RUN apt-get clean
WORKDIR /home/rstudio

EXPOSE 8787
CMD ["/init"]
ENV USER="rstudio" PASSWORD="rstudio" ROOT="TRUE"

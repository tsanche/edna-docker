# eDNA docker

This repository contains a `Dockerfile` used to build the `edna_tools` docker image including **Rstudio** web interface
and the following packages:

-   [`tidyverse`](https://www.tidyverse.org/) (`ggplot2`, `dplyr` ...)
-   [`dada2`](https://benjjneb.github.io/dada2/index.html)
-   [`OBITools`](https://pythonhosted.org/OBITools/welcome.html)
-   [`cutadapt`](https://cutadapt.readthedocs.io/en/stable/)
-   [`MKBRD`](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database)
-   [`vsearch`](https://github.com/torognes/vsearch)
-   [`ecoPCR`](https://pythonhosted.org/OBITools/scripts/ecoPCR.html)
-   [`ShortRead`](https://kasperdanielhansen.github.io/genbioconductor/html/ShortRead.html)
-   [`Biostrings`](https://kasperdanielhansen.github.io/genbioconductor/html/Biostrings.html)
-   [`phyloseq`](https://joey711.github.io/phyloseq/)
-   [`ape`](http://ape-package.ird.fr/)
-   [`mFD`](https://cmlmagneville.github.io/mFD/)
-   [`worrms`](https://github.com/ropensci/worrms)
-   [`patchwork`](https://patchwork.data-imaginist.com/)
-   [`hillR`](https://github.com/daijiang/hillR)
-   [`betapart`](https://cran.r-project.org/web/packages/betapart/index.html)
-   [`ecoPrimers`](https://git.metabarcoding.org/obitools/ecoprimers/-/wikis/home)
-   [`seqRFLP`](https://github.com/helixcn/seqRFLP)
-   [`sf`](https://r-spatial.github.io/sf/#blogs-presentations-vignettes-sp-sf-wiki)

The docker image itself is hosted on [dockerhub](https://hub.docker.com/repository/docker/thesanc/edna_tools). It allows
to run a container with usefull softwares for eDNA analysis. Independently from your operating system, the container is
launched with all the softwares pre-installed in a self-contained linux environment.

Keep in mind that any modifications made inside the container (e.g. packages newly installed or files modified) will be
lost after stopping it. Therefore, you need to use the `-v` argument (or use the dedicated field in docker desktop) to
mount the local files inside the container as explained in the tutorial bellow. Modification made to the mounted files
will not be lost and you can even create new files from inside the container in the mounted path.

The **Rstudio** web interface provides an **R** IDE (Integrated Development Environment) in addition to a terminal to
execute any bash command you may need.

## Docker tutorial

Docker images can be either used with the **command line interface** (CLI) after installing docker from your favorite
linux package manager or by using the **docker desktop** UI available on
[Mac](https://docs.docker.com/desktop/install/mac-install/),
[Windows](https://docs.docker.com/desktop/install/windows-install/) and
[Linux](https://docs.docker.com/desktop/install/linux-install/).

### Command line interface

Start the docker daemon by running:

```bash
sudo dockerd
```

In another terminal, download the image from [dockerhub](https://hub.docker.com/r/thesanc/edna_tools) and run it with
this single command:

```bash
docker run -v /path/to/your/local/file:/path/inside/the/container -p 8787:8787 thesanc/edna_tools
```

You should now be able to have access to the **Rstudio** instance by typing `localhost:8787` in your browser. You can
log in with `rstudio` both as username and password.

### Docker desktop

After installing the **docker desktop** UI, you can search for the `thesanc/edna_tools` image in the research toolbar:

![](tutorial_images/docker_desktop_home.png)

Once you have found the image, download it and run it by clicking on "run".

![](tutorial_images/docker_desktop_search.png)

A pop-up windows should open to set up the container. Click on "optional settings" to set up the port and paths:

<img src="tutorial_images/docker_desktop_run.png" width="400"/>

Fill the **Host port** field with `8787` (you can set up another port if this one is already used).

Fill the **Host path** with the file path to your files and fill the **Container path** with the path where these fill
should appear in the container. If you are using linux or macos, I recommend to use the **Host path** as **Container
path** to avoid path related issues when you are running your code inside and outside the container. On windows, you can
type in the desired **Host path** in windows style and type in `/home/rstudio` for the **Container path**, since the
path should be in unix style.

Now click on "run". You should now be able to have access to the **Rstudio** instance by typing `localhost:8787` (if you
have configured the **Host port** to `8787`) in your browser. You can log in with `rstudio` both as username and
password.
